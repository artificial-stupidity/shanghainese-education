//
//  Shanghainese_EducationUITests.swift
//  Shanghainese EducationUITests
//
//  Created by Zehua Li on 3/18/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import XCTest
@testable import Shanghainese_Education

class Shanghainese_EducationUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testChooseDifficultyView() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertNotNil(XCUIApplication().buttons["Easy"])
        XCTAssertNotNil(XCUIApplication().buttons["Medium"])
        XCTAssertNotNil(XCUIApplication().buttons["Hard"])
        XCTAssertEqual(XCUIApplication().buttons["Easy"].label, "Easy")
        XCTAssertEqual(XCUIApplication().buttons["Medium"].label, "Medium")
        XCTAssertEqual(XCUIApplication().buttons["Hard"].label, "Hard")
    }
    
    func testPracticeView() {
        XCUIApplication().buttons["Easy"].tap()
        XCTAssertNotNil(XCUIApplication().staticTexts["Practice"])
        XCTAssertNotNil(XCUIApplication().buttons["Answer"])
        XCUIApplication().buttons["Answer"].tap()
        sleep(1)
        XCTAssertNotNil(XCUIApplication().buttons["Finish"])
        XCUIApplication().buttons["Finish"].tap()
        sleep(1)
        XCTAssertNotNil(XCUIApplication().buttons["Replay"])
        XCUIApplication().buttons["Replay"].tap()
    }
    
    func testGradeView() {
        XCUIApplication().buttons["Easy"].tap()
        XCUIApplication().buttons["Answer"].tap()
        sleep(1)
        XCUIApplication().buttons["Finish"].tap()
        sleep(1)
        XCUIApplication().buttons["Submit"].tap()
        XCTAssertNotNil(XCUIApplication().staticTexts["Evaluation"])
    }
    
    func testDictionaryView() {
        XCTAssertNotNil(XCUIApplication().tabBars.buttons["Dictionary"])
        XCUIApplication().tabBars.buttons["Dictionary"].tap()
        XCTAssertNotNil(XCUIApplication().staticTexts["Dictionary"])
        sleep(2)
        
        let tablesQuery = XCUIApplication().tables
        XCTAssertNotNil(tablesQuery)
        XCTAssertNotNil(tablesQuery.staticTexts["Life chores"])
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Life chores"]/*[[".cells.staticTexts[\"Life chores\"]",".staticTexts[\"Life chores\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssertNotNil(tablesQuery.staticTexts["Custom Culture"])
        sleep(1)
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Custom Culture"]/*[[".cells.staticTexts[\"Custom Culture\"]",".staticTexts[\"Custom Culture\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssertNotNil(tablesQuery.staticTexts["除了亲戚，还请来了同事、同学。"])
        tablesQuery.staticTexts["除了亲戚，还请来了同事、同学。"].tap()
        XCTAssertNotNil(XCUIApplication().buttons["playButton"])
        XCUIApplication().buttons["playButton"].tap()
    }
    
    func testReactivate() {
        XCUIDevice.shared.press(.home)
        XCUIApplication().activate()
    }
    
    func testTerminate() {
        XCUIApplication().terminate()
    }
    
    func testPerformance() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
