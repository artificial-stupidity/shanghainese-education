//
//  CategoryTableViewController.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 3/18/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit
import Alamofire
import os

class CategoryTableViewController: UITableViewController {
    var category : Category?
    var categories = [Category]()
    
    func setCategory(category : Category) {
        self.category = category
        loadCategories()
    }
    
    private func loadCategories() {
        var url = "http://3.82.224.151/category/"
        if let c = category {
            url += String(c.id) + "/"
        }
        self.categories.removeAll()
        AF.request(url)
            .authenticate(username: "ppp", password: "ppp")
            .responseJSON { response in
            if self.category != nil, self.categories.count > 0 {
                return
            }
            if let json = response.result.value as? [AnyObject],
                self.category == nil {
                for item in json {
                    if let name = item["name"] as? String,
                        let id = item["id"] as? Int,
                        let dialogs = item["dialogs"] as? [AnyObject] {
                        var convertedDialogs : [Dialog] = [Dialog]()
                        for item in dialogs {
                            let dialog = Dialog(object: item)
                            convertedDialogs.append(dialog)
                        }
                        let category = Category(name: name, id: id, dialogs: convertedDialogs)
                        self.categories += [category]
                    }
                }
            } else if let json = response.result.value as? AnyObject,
                let subcategories = json["subcategories"] as? [AnyObject] {
                for item in subcategories {
                    if let name = item["name"] as? String,
                        let id = item["id"] as? Int {
                        var convertedDialogs : [Dialog] = [Dialog]()
                        if let dialogs = item["dialogs"] as? [AnyObject] {
                            for item in dialogs {
                                let dialog = Dialog(object: item)
                                convertedDialogs.append(dialog)
                            }
                        }
                        let category = Category(name: name, id: id, dialogs: convertedDialogs)
                        self.categories += [category]
                    }
                }
            }
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadCategories()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        guard (sender as? UITableViewCell) != nil else {
            os_log("The table cell was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        
        let detailViewController = segue.destination
            as! DictionaryViewController
        
        let myIndexPath = self.tableView.indexPathForSelectedRow!
        let row = myIndexPath.row
        detailViewController.category = categories[row]
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CategoryTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CategoryTableViewCell else {
            fatalError("The dequeued cell is not an instance of CategoryTableViewCell.")
        }
        
        let category = categories[indexPath.row]
        cell.categoryNameLabel.text = category.name

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = self.categories[indexPath.row]
        if category.dialogs.count == 0 {
            guard let viewController = storyboard?.instantiateViewController(withIdentifier: "DictionaryView") as? DictionaryViewController else { return }
            viewController.category = category
            navigationController?.pushViewController(viewController, animated: true)
        } else {
            guard let viewController = storyboard?.instantiateViewController(withIdentifier: "CategoryView") as? CategoryViewController else { return }
            viewController.category = category
            navigationController?.pushViewController(viewController, animated: true)
        }
    }

}
