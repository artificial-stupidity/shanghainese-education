//
//  TranslationViewController.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 4/15/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class TranslationViewController: UIViewController {
    @IBOutlet weak var translationLabel: UILabel!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var recorded: Bool = false
    var filename: URL = URL(string: "http://haha.com")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateHintLabel()
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.updateHintLabel()
                        self.recordButton.addTarget(self, action: #selector(self.recordTapped), for: .touchUpInside)
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
        SFSpeechRecognizer.requestAuthorization {
            [unowned self] (authStatus) in
            switch authStatus {
            case .authorized:
                print("Speech recognition authorized")
            case .denied:
                print("Speech recognition authorization denied")
            case .restricted:
                print("Not available on this device")
            case .notDetermined:
                print("Not determined")
            }
        }
    }
    
    func updateHintLabel() {
        if !recorded {
            hintLabel.text = "Please press the button to record the dialog"
        }
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        self.filename = audioFilename
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.record()
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        recorded = true
        do {
            self.translate()
        } catch {
            print("Sound initialization failed")
        }
    }
    
    func translate() {
        guard let recognizer = SFSpeechRecognizer(locale: Locale(identifier: "wuu-CN")) else {
            print("Speech recognition not available for specified locale")
            return
        }
        
        if !recognizer.isAvailable {
            print("Speech recognition not currently available")
            return
        }
        
        let request = SFSpeechURLRecognitionRequest(url: filename)
        recognizer.recognitionTask(with: request) {
            [unowned self] (result, error) in
            guard let result = result else {
                print("There was an error transcribing that file")
                return
            }
            
            if result.isFinal {
                self.translationLabel.text =
                    result.bestTranscription.formattedString
                self.hintLabel.text = "Please press the button to record the dialog"
            }
        }
    }
    
    @objc func recordTapped() {
        if recorded == false {
            startRecording()
            hintLabel.text = "Recording..."
            recorded = true
        } else {
            finishRecording(success: true)
            hintLabel.text = "Translating..."
            recorded = false
        }
    }

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}
