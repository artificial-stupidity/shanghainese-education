//
//  GradeViewController.swift
//  Shanghainese Education
//
//  Created by 胡于启 on 2019/3/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

class GradeViewController: UIViewController {
    var dialog: Dialog?
    var evaluation: Evaluation?
    var translation: String?
    var correctAudioPlayer: AVAudioPlayer?
    var responseAudioPlayer: AVAudioPlayer?
    var responseFilename: URL = URL.init(string: "http://google.com")!
    
    @IBOutlet weak var chineseLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var romanizedPhonesLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var replayButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        chineseLabel.text = dialog?.chineseTranslation
        englishLabel.text = dialog?.englishTranslation
        romanizedPhonesLabel.text = dialog?.romanizedPhones
        requestEvaluation()
        loadAudio()
    }
    
    func requestEvaluation() {
        let url: String = "http://3.82.224.151/"
        AF.request(url + "evaluation/" + String(evaluation!.id) + "/", method: .put, parameters: ["translation": self.translation])
            .authenticate(username: "ppp", password: "ppp")
            .responseJSON{ response in
            if let json = response.result.value as? AnyObject,
                let grade = json["grade"] as? Int {
                self.setGrade(grade: grade)
            }
        }
    }
    
    func setGrade(grade: Int) {
        gradeLabel.text = String(grade)
        if grade < 30 {
            gradeLabel.textColor = UIColor.red
        } else if grade < 60 {
            gradeLabel.textColor = UIColor.orange
        } else if grade >= 60 {
            gradeLabel.textColor = UIColor(red: 0.195, green: 0.574, blue: 0.164, alpha: 1.0)
        }
    }

    @IBAction func correctAudioButtonPressed(_ sender: Any) {
        self.correctAudioPlayer?.play()
    }
    
    @IBAction func responseAudioButtonPressed(_ sender: Any) {
        self.responseAudioPlayer?.play()
    }

    func loadAudio() {
        do {
            self.responseAudioPlayer = try AVAudioPlayer(contentsOf: self.responseFilename)
        } catch {
            print("Response sound initialization failed")
        }
        AF.download((dialog?.audio.absoluteString)!).responseData { response in
            if let data = response.result.value {
                do {
                    self.correctAudioPlayer = try AVAudioPlayer(data: data)
                } catch {
                    print("Correct sound initialization failed")
                }
            }
        }
    }

}
