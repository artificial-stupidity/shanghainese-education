//
//  Category.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 3/19/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit

class Category {
    var name: String
    var id: Int
    var dialogs: [Dialog]
    
    init(name: String, id: Int, dialogs: [Dialog] = [Dialog]()) {
        self.name = name
        self.id = id
        self.dialogs = dialogs
    }
}
