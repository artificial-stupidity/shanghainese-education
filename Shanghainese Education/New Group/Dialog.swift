//
//  Dialog.swift
//  Shanghainese Education
//
//  Created by 胡于启 on 2019/3/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import Foundation

struct Dialog{
    
    var id: Int
    var difficulty: String
    var chineseTranslation: String
    var englishTranslation: String
    var romanizedPhones: String
    var audio: URL
    
    init(id: Int, difficulty: String, chineseTranslation: String = "", englishTranslation: String = "", romanizedPhones: String = "", audio: URL = URL(string: "Null")!) {
        self.id = id
        self.difficulty = difficulty
        self.chineseTranslation = chineseTranslation
        self.englishTranslation = englishTranslation
        self.romanizedPhones = romanizedPhones
        self.audio = URL(string: "http://3.82.224.151/dialog/" + String(id) + "/?alt=media")!
    }
    
    init(object: AnyObject) {
        if let id = object["id"] as? Int,
            let difficulty = object["difficulty"] as? Int,
            let chineseTranslation = object["chinese_translation"] as? String,
            let englishTranslation = object["english_translation"] as? String,
            let romanizedPhones = object["romanized_phones"] as? String {
            var difficultyStr = "Hardest"
            if difficulty == 1 { difficultyStr = "Easy" }
            else if difficulty == 2 { difficultyStr = "Medium" }
            else if difficulty == 3 { difficultyStr = "Hard" }

            self.id = id
            self.difficulty = difficultyStr
            self.chineseTranslation = chineseTranslation
            self.englishTranslation = englishTranslation
            self.romanizedPhones = romanizedPhones
            self.audio = URL(string: "http://3.82.224.151/dialog/" + String(id) + "/?alt=media")!
        } else {
            self.id = -1
            self.difficulty = "Hardest"
            self.chineseTranslation = "Null"
            self.englishTranslation = "Null"
            self.romanizedPhones = "Null"
            self.audio = URL(string: "Null")!
        }
    }

}
