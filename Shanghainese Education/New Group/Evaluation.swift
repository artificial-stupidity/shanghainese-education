//
//  Evaluation.swift
//  Shanghainese Education
//
//  Created by 胡于启 on 2019/3/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import Foundation

class Evaluation{
    
    var id: Int
    var grade: Int = 0
    
    init(id: Int) {
        self.id = id
    }
    
}
