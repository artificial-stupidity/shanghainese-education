//
//  DictionaryViewController.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 4/9/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit

class DictionaryViewController: UIViewController {
    var category : Category?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let c = category as? Category {
            let tableViewController = self.children.last as! CategoryTableViewController
            tableViewController.setCategory(category: c)
            titleLabel.text = category?.name
        }
    }
}
