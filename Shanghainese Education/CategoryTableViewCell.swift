//
//  CategoryTableViewCell.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 3/18/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
