//
//  DialogViewController.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 4/10/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

class DialogViewController: UIViewController {
    var dialog: Dialog?
    var sound: AVAudioPlayer?
    @IBOutlet weak var difficultyLabel: UILabel!
    @IBOutlet weak var chineseLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var romanizedPhonesLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    @IBAction func buttonOnPress(_ sender: Any) {
        sound?.play()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        difficultyLabel.text = dialog?.difficulty
        chineseLabel.text = dialog?.chineseTranslation
        englishLabel.text = dialog?.englishTranslation
        romanizedPhonesLabel.text = dialog?.romanizedPhones
        styleDifficulty()
        AF.download((dialog?.audio.absoluteString)!).responseData { response in
            if let data = response.result.value {
                do {
                    self.sound = try AVAudioPlayer(data: data)
                } catch {
                    print("Sound initialization failed")
                }
            }
        }
    }
    
    func styleDifficulty() {
        if dialog?.difficulty == "Hard" {
            difficultyLabel.textColor = UIColor.red
        } else if dialog?.difficulty == "Medium" {
            difficultyLabel.textColor = UIColor.orange
        } else if dialog?.difficulty == "Easy" {
            difficultyLabel.textColor = UIColor(red: 0.195, green: 0.574, blue: 0.164, alpha: 1.0)
        }
    }

}
