//
//  DictionaryViewController.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 4/9/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {
    var category : Category?
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let c = category as? Category {
            let tableViewController = self.children.last as! DialogTableViewController
            tableViewController.setCategory(category: c)
            categoryNameLabel.text = category?.name
        }
    }
}
