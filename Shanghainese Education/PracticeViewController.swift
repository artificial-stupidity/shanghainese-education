//
//  PracticeViewController.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 3/20/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit
import CoreFoundation
import AVFoundation
import Alamofire
import Speech
import os

class PracticeViewController: UIViewController {
    @IBOutlet weak var firstDialog: UIView!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var chineseDialog: UILabel!
    @IBOutlet weak var englishDialog: UILabel!
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer?
    var recorded: Bool = false
    var filename: URL = URL.init(string: "http://google.com")!
    var translation: String = ""
    var training: AnyObject? = nil
    var dialog: Dialog? = nil
    var evaluation: Evaluation? = nil
    var difficulty: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recordingSession = AVAudioSession.sharedInstance()
        loadSession()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
        SFSpeechRecognizer.requestAuthorization {
            [unowned self] (authStatus) in
            switch authStatus {
            case .authorized:
                print("Speech recognition authorized")
            case .denied:
                print("Speech recognition authorization denied")
            case .restricted:
                print("Not available on this device")
            case .notDetermined:
                print("Not determined")
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        styleButtons(buttons: [answerButton, submitButton])
    }
    
    func loadSession() {
        let url: String = "http://3.82.224.151/"
        AF.request(url + "session/", method: .post, parameters: ["difficulty": difficulty])
            .authenticate(username: "ppp", password: "ppp")
            .responseJSON { response in
            if let json = response.result.value as? AnyObject,
                let difficulty = json["difficulty"] as? Int {
                AF.request(url + "session/")
                    .authenticate(username: "ppp", password: "ppp")
                    .responseJSON { res in
                    if let resList = res.result.value as? [AnyObject],
                        let session = resList[0] as? AnyObject,
                        let trainings = session["trainings"] as? [AnyObject],
                        let training = trainings[0] as? AnyObject,
                        let evaluation = training["evaluation"] as? AnyObject,
                        let evaluationId = evaluation["id"] as? Int,
                        let dialogId = training["dialog"] as? Int {
                            self.evaluation = Evaluation(id: evaluationId)
                            AF.request(url + "dialog/" + String(dialogId) + "/")
                                .authenticate(username: "ppp", password: "ppp")
                                .responseJSON { s in
                                if let d = s.result.value as? AnyObject {
                                    self.dialog = Dialog(object: d)
                                    self.chineseDialog.text = self.dialog?.chineseTranslation
                                    self.englishDialog.text = self.dialog?.englishTranslation
                                }
                            }
                        }
                    }
            } else {
                self.deleteSession()
            }
        }
    }
    
    func deleteSession() {
        let url: String = "http://3.82.224.151/session/"
        AF.request(url, method: .get)
            .authenticate(username: "ppp", password: "ppp")
            .responseJSON { response in
            if let sessionList = response.result.value as? [AnyObject] {
                for session in sessionList {
                    if let id = session["id"] as? Int {
                        AF.request(url + String(id) + "/", method: .delete)
                            .authenticate(username: "ppp", password: "ppp")
                            .responseJSON{ r in
                            self.loadSession()
                        }
                    }
                }
            }
        }
    }
    
    func styleDialogContainers(dialogs: [UIView], type: String) {
        for dialog in dialogs {
            let gradient = CAGradientLayer.init()
            gradient.frame = dialog.bounds
            dialog.layer.cornerRadius = 8
            if type == "left" {
                dialog.layer.borderWidth = 1
                dialog.layer.borderColor = UIColor(displayP3Red: 0.5898, green: 0.5898, blue: 0.5898, alpha: 1.0).cgColor
                gradient.colors = [
                    UIColor(displayP3Red: 0.9922, green: 0.9922, blue: 0.9922, alpha: 1.0),
                    UIColor(displayP3Red: 0.8438, green: 0.8438, blue: 0.8438, alpha: 1.0)
                ]
            } else {
                gradient.colors = [
                    UIColor(displayP3Red: 0.2891, green: 0.5625, blue: 0.8828, alpha: 0.7),
                    UIColor(displayP3Red: 0.2891, green: 0.5625, blue: 0.8828, alpha: 0.3),
                ]
            }
            gradient.startPoint = CGPoint.zero
            gradient.endPoint = CGPoint(x: 1, y: 1)
            dialog.layer.insertSublayer(gradient, at: 0)
            dialog.layer.addSublayer(gradient)
        }
    }
    
    func loadRecordingUI() {
        answerButton.setTitle("Answer", for: .normal)
        answerButton.addTarget(self, action: #selector(recordTapped), for: .touchUpInside)
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        self.filename = audioFilename
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.record()
            
            answerButton.setTitle("Finish", for: .normal)
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        recorded = true
        answerButton.setTitle("Replay", for: .normal)
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: filename)
            self.translate()
        } catch {
            print("Sound initialization failed")
        }
    }
    
    func translate() {
        guard let recognizer = SFSpeechRecognizer(locale: Locale(identifier: "wuu-CN")) else {
            print("Speech recognition not available for specified locale")
            return
        }
        
        if !recognizer.isAvailable {
            print("Speech recognition not currently available")
            return
        }
        
        let request = SFSpeechURLRecognitionRequest(url: filename)
        recognizer.recognitionTask(with: request) {
            [unowned self] (result, error) in
            guard let result = result else {
                print("There was an error transcribing that file")
                return
            }
            
            if result.isFinal {
                self.translation =
                    result.bestTranscription.formattedString
            }
        }
    }
    
    @objc func recordTapped() {
        if audioRecorder == nil && recorded == false {
            startRecording()
        } else {
            if recorded {
                audioPlayer?.play()
            } else {
                finishRecording(success: true)
            }
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }

    @IBAction func submitButtonOnPress(_ sender: Any) {
        if !recorded { return }
        guard let homeViewController = storyboard?.instantiateViewController(withIdentifier: "ChooseDifficultyView") as? ChooseDifficultyViewController else { return }
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "EvaluationView") as? GradeViewController else { return }
        viewController.dialog = self.dialog
        viewController.evaluation = self.evaluation
        viewController.translation = self.translation
        viewController.responseFilename = self.filename
        navigationController?.setViewControllers([homeViewController, viewController], animated: true)
    }
    
    func styleButtons(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = 12
        }
    }
}
