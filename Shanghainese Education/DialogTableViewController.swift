//
//  CategoryTableViewController.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 3/18/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit
import Alamofire
import os

class DialogTableViewController: UITableViewController {
    var category : Category?
    var dialogs = [Dialog]()
    
    func setCategory(category : Category) {
        self.category = category
        loadDialogs()
    }

    private func loadDialogs() {
        self.dialogs.removeAll()
        guard self.category != nil else { return }
        let url = "http://3.82.224.151/category/" + String(self.category!.id) + "/"
        AF.request(url, method: .get).authenticate(username: "ppp", password: "ppp").responseJSON{ response in
            if let json = response.result.value as? AnyObject,
                let dialogs = json["dialogs"] as? [AnyObject] {
                for dialog in dialogs {
                    self.dialogs.append(Dialog(object: dialog))
                }
            }
            self.tableView.reloadData()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDialogs()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dialogs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "DialogTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DialogTableViewCell  else {
            fatalError("The dequeued cell is not an instance of CategoryTableViewCell.")
        }
        
        let dialog = dialogs[indexPath.row]
        cell.dialogNameLabel.text = dialog.englishTranslation
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dialog = self.dialogs[indexPath.row]
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "DialogView") as? DialogViewController else { return }
        viewController.dialog = dialog
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
