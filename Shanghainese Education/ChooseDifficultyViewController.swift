//
//  FirstViewController.swift
//  Shanghainese Education
//
//  Created by Zehua Li on 3/18/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import UIKit

class ChooseDifficultyViewController: UIViewController {
    
    @IBOutlet weak var easyLabel: UILabel!
    @IBOutlet weak var mediumLabel: UILabel!
    @IBOutlet weak var HardLabel: UILabel!
    @IBOutlet weak var easyButton: UIButton!
    @IBOutlet weak var mediumButton: UIButton!
    @IBOutlet weak var hardButton: UIButton!
    @IBOutlet weak var stackContainerView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stackContainerView.alignment = .fill
        styleButtons(buttons: [easyButton, mediumButton, hardButton])
    }
    
    func styleButtons(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = 12
            button.frame.size = CGSize(width: 200.0, height: 40.0)
            button.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    @IBAction func easyButtonPressed(_ sender: Any) {
        startPractice(difficulty: 1)
    }
    
    @IBAction func mediumButtonPressed(_ sender: Any) {
        startPractice(difficulty: 2)
    }

    @IBAction func hardButtonPressed(_ sender: Any) {
        startPractice(difficulty: 3)
    }

    func startPractice(difficulty: Int) {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "PracticeView") as? PracticeViewController else { return }
        viewController.difficulty = difficulty
        navigationController?.pushViewController(viewController, animated: true)
    }
}
