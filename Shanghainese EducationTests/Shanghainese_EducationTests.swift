//
//  Shanghainese_EducationTests.swift
//  Shanghainese EducationTests
//
//  Created by Zehua Li on 3/18/19.
//  Copyright © 2019 Ultimate Shanghainese Odyssey. All rights reserved.
//

import XCTest
@testable import Shanghainese_Education
@testable import Alamofire

class Shanghainese_EducationTests: XCTestCase {
    var dialogTest: Dialog! = nil
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        dialogTest = nil
    }

    func testDialogIsCreatedWithTwoParameters() {
        dialogTest = Dialog(id: 1, difficulty: "Hard")
        XCTAssertEqual(dialogTest.id, 1)
        XCTAssertEqual(dialogTest.difficulty, "Hard")
    }
    
    func testDialogIsCreatedWithSixParameters() {
        dialogTest = Dialog(id: 2, difficulty: "Good", chineseTranslation: "大家好，我是小达达。", englishTranslation: "Hello everyone, my name is little Dada.", romanizedPhones: "da gha ho u zy shiao da da", audio: URL(string: "http://3.82.224.151/dialog/2/?alt=media")!)
        XCTAssertEqual(dialogTest.id, 2)
        XCTAssertEqual(dialogTest.difficulty, "Good")
        XCTAssertEqual(dialogTest.chineseTranslation, "大家好，我是小达达。")
        XCTAssertEqual(dialogTest.englishTranslation, "Hello everyone, my name is little Dada.")
        XCTAssertEqual(dialogTest.romanizedPhones, "da gha ho u zy shiao da da")
        XCTAssertNotNil(dialogTest.audio)
        XCTAssertEqual(dialogTest.audio.absoluteString, "http://3.82.224.151/dialog/2/?alt=media")
    }
    
    func testHTTPRequest() {
        let promise = expectation(description: "Status code: 200")
        AF.request("http://3.82.224.151/dialog/3/")
            .authenticate(username: "ppp", password: "ppp")
            .responseJSON { response in
            XCTAssertNotNil(response)
            if let json = response.result.value as? AnyObject {
                XCTAssertNotNil(json)
                self.dialogTest = Dialog(object: json)
                XCTAssertEqual(self.dialogTest.id, 3)
                promise.fulfill()
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testHTTPRequestFailure() {
        let promise = expectation(description: "Status code: 200")
        AF.request("http://3.82.224.151/dialog/-1/")
            .authenticate(username: "ppp", password: "ppp")
            .responseJSON { response in
                XCTAssertNotNil(response.error)
                promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testPerformance() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
